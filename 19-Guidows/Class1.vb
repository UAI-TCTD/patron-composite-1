﻿Imports _19_Guidows

Public MustInherit Class Componente

    Private _nombre As String
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public MustOverride Sub AgregarComponente(compomente As Componente)
    Public MustOverride Sub RemoverComponente(componente As Componente)
    Public MustOverride Function Dibujar() As String

    Public Overrides Function ToString() As String
        Return Me.Dibujar()
    End Function
End Class

Public Class CheckBox
    Inherits Componente
    Private _estado As Boolean
    Public Property estado() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property

    Public Overrides Sub AgregarComponente(compomente As Componente)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Function Dibujar() As String
        Dim str As String
        str = "CheckBox " + nombre + ": "
        If estado = True Then
            String.Concat(str, "True")
        Else
            String.Concat(str, "False")
        End If
        Return str
    End Function

    Public Overrides Sub RemoverComponente(componente As Componente)
        Throw New NotImplementedException()
    End Sub
End Class

Public Class Boton
    Inherits Componente

    Private _texto As String
    Public Property texto() As String
        Get
            Return _texto
        End Get
        Set(ByVal value As String)
            _texto = value
        End Set
    End Property
    Public Sub New()
        nombre = "boton"
    End Sub
    Public Overrides Sub AgregarComponente(compomente As Componente)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Sub RemoverComponente(componente As Componente)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Function Dibujar() As String
        Dim str As String
        str = nombre + ": " + texto
        Return str
    End Function
End Class

Public Class TextBox
    Inherits Componente

    Private _texto As String
    Public Property texto() As String
        Get
            Return _texto
        End Get
        Set(ByVal value As String)
            _texto = value
        End Set
    End Property
    Public Sub New()
        nombre = "TextBox"
    End Sub
    Public Overrides Sub AgregarComponente(componente As Componente)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Sub RemoverComponente(componente As Componente)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Function Dibujar() As String
        Dim str As String
        str = nombre + ": " + texto
        Return str
    End Function
End Class

Public MustInherit Class Composite
    Inherits Componente

    Private _lista As List(Of Componente)
    Public Property lista() As List(Of Componente)
        Get
            Return _lista
        End Get
        Set(ByVal value As List(Of Componente))
            _lista = value
        End Set
    End Property

End Class

Public Class Frame
    Inherits Composite

    Public Sub New()
        nombre = "Frame"
    End Sub

    Public Overrides Sub AgregarComponente(componente As Componente)
        MyBase.lista.Add(componente)
    End Sub

    Public Overrides Sub RemoverComponente(componente As Componente)
        MyBase.lista.Remove(componente)
    End Sub

    Public Overrides Function Dibujar() As String
        Dim str As String
        str = nombre + ":|------------ "
        For Each i In MyBase.lista
            String.Concat(str, " - ", i.nombre)
        Next
        Return str
    End Function
End Class

Public Class Ventana
    Inherits Composite

    Public Sub New()
        nombre = "Ventana"
    End Sub
    Public Overrides Sub AgregarComponente(componente As Componente)
        MyBase.lista.Add(componente)
    End Sub

    Public Overrides Sub RemoverComponente(componente As Componente)
        MyBase.lista.Remove(componente)
    End Sub

    Public Overrides Function Dibujar() As String
        Dim str As String
        str = nombre + ":|------------ "
        For Each i In MyBase.lista
            String.Concat(str, " - ", i.nombre)
        Next
        Return str
    End Function
End Class
