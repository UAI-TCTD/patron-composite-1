﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtTextBox = New System.Windows.Forms.TextBox()
        Me.chkCheckBox = New System.Windows.Forms.CheckBox()
        Me.btnFrame = New System.Windows.Forms.Button()
        Me.btnVentana = New System.Windows.Forms.Button()
        Me.btnBoton = New System.Windows.Forms.Button()
        Me.btnMostrar = New System.Windows.Forms.Button()
        Me.lbSalida = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'txtTextBox
        '
        Me.txtTextBox.Location = New System.Drawing.Point(12, 111)
        Me.txtTextBox.Multiline = True
        Me.txtTextBox.Name = "txtTextBox"
        Me.txtTextBox.Size = New System.Drawing.Size(131, 69)
        Me.txtTextBox.TabIndex = 10
        '
        'chkCheckBox
        '
        Me.chkCheckBox.AutoSize = True
        Me.chkCheckBox.Location = New System.Drawing.Point(12, 87)
        Me.chkCheckBox.Name = "chkCheckBox"
        Me.chkCheckBox.Size = New System.Drawing.Size(75, 17)
        Me.chkCheckBox.TabIndex = 9
        Me.chkCheckBox.Text = "CheckBox"
        Me.chkCheckBox.UseVisualStyleBackColor = True
        '
        'btnFrame
        '
        Me.btnFrame.Location = New System.Drawing.Point(150, 12)
        Me.btnFrame.Name = "btnFrame"
        Me.btnFrame.Size = New System.Drawing.Size(131, 23)
        Me.btnFrame.TabIndex = 8
        Me.btnFrame.Text = "Frame"
        Me.btnFrame.UseVisualStyleBackColor = True
        '
        'btnVentana
        '
        Me.btnVentana.Location = New System.Drawing.Point(12, 12)
        Me.btnVentana.Name = "btnVentana"
        Me.btnVentana.Size = New System.Drawing.Size(131, 23)
        Me.btnVentana.TabIndex = 7
        Me.btnVentana.Text = "Ventana"
        Me.btnVentana.UseVisualStyleBackColor = True
        '
        'btnBoton
        '
        Me.btnBoton.Location = New System.Drawing.Point(12, 57)
        Me.btnBoton.Name = "btnBoton"
        Me.btnBoton.Size = New System.Drawing.Size(131, 23)
        Me.btnBoton.TabIndex = 6
        Me.btnBoton.Text = "Boton"
        Me.btnBoton.UseVisualStyleBackColor = True
        '
        'btnMostrar
        '
        Me.btnMostrar.Location = New System.Drawing.Point(150, 245)
        Me.btnMostrar.Name = "btnMostrar"
        Me.btnMostrar.Size = New System.Drawing.Size(231, 31)
        Me.btnMostrar.TabIndex = 12
        Me.btnMostrar.Text = "Mostar"
        Me.btnMostrar.UseVisualStyleBackColor = True
        '
        'lbSalida
        '
        Me.lbSalida.FormattingEnabled = True
        Me.lbSalida.Location = New System.Drawing.Point(150, 57)
        Me.lbSalida.Name = "lbSalida"
        Me.lbSalida.Size = New System.Drawing.Size(231, 186)
        Me.lbSalida.TabIndex = 13
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(385, 281)
        Me.Controls.Add(Me.lbSalida)
        Me.Controls.Add(Me.btnMostrar)
        Me.Controls.Add(Me.txtTextBox)
        Me.Controls.Add(Me.chkCheckBox)
        Me.Controls.Add(Me.btnFrame)
        Me.Controls.Add(Me.btnVentana)
        Me.Controls.Add(Me.btnBoton)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents chkCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents btnFrame As System.Windows.Forms.Button
    Friend WithEvents btnVentana As System.Windows.Forms.Button
    Friend WithEvents btnBoton As System.Windows.Forms.Button
    Friend WithEvents btnMostrar As System.Windows.Forms.Button
    Friend WithEvents lbSalida As System.Windows.Forms.ListBox
End Class
